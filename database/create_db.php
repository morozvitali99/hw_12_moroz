<?php
    require_once    $_SERVER['DOCUMENT_ROOT'] . "/database/connect.php";
// создаем таблицу с заголовками 'store'
    include_once    $_SERVER['DOCUMENT_ROOT'] . "/template/header.php";
        try
{
    $sql = 'CREATE TABLE        store (
        id                      INT NOT NULL      AUTO_INCREMENT PRIMARY KEY,
        title                   VARCHAR(255),
        price                   Float,
        description_            VARCHAR(255),
        type_                   VARCHAR(255)
        )
        DEFAULT CHARACTER SET       utf8
                                    ENGINE=InnoDB;';
        $db->exec($sql);    
}

            catch(Exception $except)
        {
            ?>
            
            <!-- это информационнаое поле на странице для отображения служебной информации -->
                        <div    class="alert alert-warning"
                                role="alert">
                                    <?='table store alredy inserted, return to '?>
                        <a      href="/index.php">Homepage</a>
                    </div>
            <?php
//            echo $except->getMessage();
            die();
        }
?>
        <div    class="alert alert-warning"
                role="alert">
        <?='table store created, return to '?>
        <a      href="/index.php">Homepage</a>
        </div>
<?php
    include_once    $_SERVER['DOCUMENT_ROOT'] . "/template/footer.php";
?>