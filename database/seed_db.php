<?php
    require_once    $_SERVER['DOCUMENT_ROOT'] . "/database/connect.php";
    include_once    $_SERVER['DOCUMENT_ROOT'] . "/template/header.php";
?>
<!-- это информационнаое поле на странице для отображения служебной информации -->
            <div    class="alert alert-warning"
                    role="alert">
                        <?='Inserting data to db, return to '?>
            <a      href="/index.php">Homepage</a>
        </div>
<?php
    include_once    $_SERVER['DOCUMENT_ROOT'] . "/template/footer.php";
// массив для отправки в бд
$storeArray = [
    [
        'title' => "Samsung Galaxy",
        'price' => 200,
        'description_' => "The Samsung Galaxy with the latest software updates is a great basic phone with sufficient memory and storage",
        'id' => 1,
        'type_' => 'phone'
    ],
    [
        'title' => "Samsung NP",
        'price' => 300,
        'description_' => "the Series 5 is built around an AMD processor, a change that affects not only price but also overall performance in ways most consumers are not likely to benefit from or appreciate",
        'id' => 2,
        'type_' => 'laptop'
    ],
    [
        'title' => "Samsung Galaxy Watch",
        'price' => 300,
        'description_' => "The SAMSUNG Galaxy Watch is designed to help you learn more So you can achieve more. It goes beyond tracking steps And calories",
        'id' => 3,
        'type_' => 'watch'
    ],
    [
        'title' => "Xiaomi Redmi Note",
        'price' => 400,
        'description_' => " The smartphone comes in 6.53 inches size and the display is IPS LCD capacitive touchscreen that provides 1080 x 2400 pixels resolution",
        'id' => 4,
        'type_' => 'phone'
    ],
    [
        'title' => "Xiaomi RedmiBook",
        'price' => 800,
        'description_' => "Xiaomi has unveiled its latest ultrabook, the RedmiBook. The laptop features a 2.5K display with an 89% screen-to-body ratio",
        'id' => 5,
        'type_' => 'laptop'
    ],
    [
        'title' => "Xiaomi Watch",
        'price' => 100,
        'description_' => "Xiaomi defines the long-term scenario as two half-hour long training sessions with GPS, a permanently activated step counter",
        'id' => 6,
        'type_' => 'watch'
    ],
    [
        'title' => "Apple iPhone",
        'price' => 1700,
        'description_' => "The iPhone 12 features a 6.1-inch (15 cm) display with Super Retina XDR OLED technology at a resolution of 2532×1170 pixels and a pixel density of about 460",
        'id' => 7,
        'type_' => 'phone'
    ],
    [
        'title' => "Apple MacBook",
        'price' => 1500,
        'description_' => "MacBook Pro with Apple M1 chip: Testing conducted by Apple in October 2020 using preproduction 13-inch MacBook Pro systems with Apple M1 chip",
        'id' => 8,
        'type_' => 'laptop'
    ],
    [
        'title' => "Apple Watch",
        'price' => 500,
        'description_' => "The Sport Band is made from a durable yet surprisingly soft high-performance fluoroelastomer, with an innovative pin-and-tuck closure",
        'id' => 9,
        'type_' => 'watch'
    ],
    [
        'title' => "Google Pixel",
        'price' => 1100,
        'description_' => "Google Pixel smartphone was launched on 30th September 2020. The phone comes with a 6.00-inch touchscreen display",
        'id' => 10,
        'type_' => 'phone'
    ],
    [
        'title' => "Google Pixelbook Go",
        'price' => 1300,
        'description_' => "Browser-based, cloud-powered, and boots up in seconds, which allows you to connect to the internet almost instantly",
        'id' => 11,
        'type_' => 'laptop'
    ],
    [
        'title' => "Suunto Sandstone",
        'price' => 500,
        'description_' => "Sandstone is a smartwatch with versatile GPS sports experience. Its powered with Wear OS by Google",
        'id' => 12,
        'type_' => 'watch'
    ],
            ];

try {// заполняем массив данными 
        foreach($storeArray as $store)
        {
        $sql = "INSERT INTO store SET 
            title                =   '". $store      ['title']."',
            price                =   '". $store      ['price']."',
            description_         =   '". $store      ['description_']."',
            id                   =   '". $store      ['id']."',
            type_                =   '". $store      ['type_']."'
        ";

    $response = $db->exec($sql); //выполняем заливку таблицы в бд
        }
    }catch(exception $errorMessage)
        {
        echo '<div class="alert alert-warning" role="alert">
                Duplicate entry, Data already inserted
            </div>
        ';
    }


        include_once  $_SERVER['DOCUMENT_ROOT'] . "/template/footer.php";
?>