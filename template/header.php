<!DOCTYPE html>
  <html lang="en">
  <head>
      <meta     charset="UTF-8">
      <meta     http-equiv="X-UA-Compatible"
                content="IE=edge">
      <meta     name="viewport"
                content="width=device-width, initial-scale=1.0">
      <title>HILLEL HOMEWORK MOROZ 12 CRUD</title>
      <meta name="description" content="Homework hillel #12 | CRUD PHP">
      <link     href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  </head>
<body>
<!--меню-->
      <H1> HILLEL HOMEWORK MOROZ 12 CRUD </H1>
  <ul class="nav">
      <li       class="nav-item">
            <a  class="nav-link active"
                aria-current="page"
                href="/index.php">HOME_PAGE</a>
      </li>
      <li       class="nav-item">
            <a  class="nav-link"
                href="/database/create_db.php">CREATE_DB</a>
      </li>
      <li       class="nav-item">
            <a  class="nav-link"
                href="/database/seed_db.php">SEED_DB</a>
      </li>
  </ul>

