<?php
// наследующий класс watch

class WatchStoreClass extends StoreClass{
    
    public $type_ = 'watch';

    public function __construct    ($title,
                                    $price,
                                    $description_,
                                    $id
                                )
    {
                                      
    parent::__construct(            $title,
                                    $price,
                                    $description_,
                                    $id,
                                    $type_);
    $this->type_ = $type_;
// вывод товара
    }
    public function getStoreCard(){
        echo    '<div class="card text-white bg-info mb-3;">';
        echo        '<div class="card-header">';
        echo            $this->title;
        echo                    '</div>';
        echo           '<div class="card-body">';
        echo            '<h5 class="card-title">';
        echo                'WATCH';
        echo                '</h5>';
        echo                '<p class="card-text">';
        echo                ' <img src="/svg/watch.svg" alt="watch image" height="200px" hspace="50px"> <br> ';
        echo                parent::getStoreCard() . '<br>';
        echo                ' </p>';
        echo             '</div>';
        echo          '</div>';
    }

}
?>