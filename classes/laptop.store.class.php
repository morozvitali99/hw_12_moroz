<?php
// наследующий класс laptop

class LaptopStoreClass extends StoreClass{
    
    public $type_ = 'laptop';

    public function __construct    ($title,
                                    $price,
                                    $description_,
                                    $id
                                )
    {
                                      
    parent::__construct(            $title,
                                    $price,
                                    $description_,
                                    $id,
                                    $type_);
    $this->type_ = $type_;


// вывод товара
    }
    public function getStoreCard(){
        echo    '<div class="card text-white bg-primary mb-3;">';
        echo        '<div class="card-header">';
        echo            $this->title;
        echo                    '</div>';
        echo           '<div class="card-body">';
        echo            '<h5 class="card-title">';
        echo                'LAPTOP';
        echo                '</h5>';
        echo                '<p class="card-text">';
        echo                ' <img src="/svg/laptop.svg" alt="laptop image" height="200px"> <br> ';
        echo                parent::getStoreCard() . '<br>';
        echo                ' </p>';
        echo             '</div>';
        echo          '</div>';
    }


      
}
?>