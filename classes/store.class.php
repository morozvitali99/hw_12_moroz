<?php
// наследуемый класс
    class StoreClass
    {
            public  $title = '';
            public  $price = 0;
            public  $description_ = '';
            public  $id = 1;

    public function __construct(    $title,
                                    $price,
                                    $description_,
                                    $id)
        {
            $this->title              =   $title;
            $this->price              =   $price;
            $this->description_       =   $description_;
            $this->id                 =   $id;
        }

    static public function delete($id, $db){
            try{
                $sql = "DELETE FROM store WHERE id=:id";
                $statement = $db->prepare($sql);
                $statement->bindValue(':id', $id);
                $statement->execute();
            }catch(Exception $e){
                die('Error deleting item<br>'.$e->getMessage());
            }
        }

    public function edit(PDO $db){
        try{
            $sql = "UPDATE store SET
                title=:title,
                price=:price,
                description_=:description_,
                id=:id,
                type_=:type_
             WHERE id = :id";
            $statement = $db->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':price', $this->price);
            $statement->bindValue(':description_', $this->description_);
            $statement->bindValue(':id', $this->id);
            $statement->bindValue(':type_', $this->type_);
            $statement->execute();

        }catch(Exception $e){
            die('Error updating note<br>'. $e->getMessage() );
        }
    }

    

public function update(PDO $db){
    try{
        $sql = "UPDATE store SET
            title=:title,
            price=:price,
            description_=:description_
         WHERE id = :id";
        $statement = $db->prepare($sql);
        $statement->bindValue(':title', $this->title);
        $statement->bindValue(':price', $this->price);
        $statement->bindValue(':description_', $this->description_);
        $statement->bindValue(':id', $this->id);
        $statement->execute();
  
    }catch(Exception $e){
        die('Error updating<br>'. $e->getMessage() );
    }
  }

// тут напишем текст визитки одной строкой
    public function getStoreCard()
        {
        return  ' Title: ' . $this->title . '<br>' .
                ' Price: ' . $this->price .  " &#36; " .  '<br>' .
                ' Description: ' . $this->description_ . '<br>' .
                
                            // кнопка DELETE
                ' <form style="display:inline-block" method="post" action="/CRUD/delete.php">
                <input type="hidden" name="id" value="'. $this->id .'">
                <button class="btn btn-danger">Delete</button>
                </form>' .

                            // кнопка SHOW
                ' <form style="display:inline-block" method="post" action="/CRUD/show.php">
                <input type="hidden" name="id" value="'. $this->id .'">
                <button class="btn btn-warning">Show</button>
                </form>' .

                            // ткнопка SHOW
                ' <form style="display:inline-block" method="post" action="/CRUD/edit.php">
                <input type="hidden" name="id" value="'. $this->id .'">
                <button class="btn btn-light">Edit</button>
                </form>' 
; 
        }
    }
?>