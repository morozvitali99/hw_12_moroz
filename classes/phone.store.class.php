<?php
// наследующий класс phone

class PhoneStoreClass extends StoreClass{
    
    public $type_ = 'phone';

    public function __construct    ($title,
                                    $price,
                                    $description_,
                                    $id
                                )
    {
                                      
    parent::__construct(            $title,
                                    $price,
                                    $description_,
                                    $id,
                                    $type_);
    $this->type_ = $type_;
// вывод товара
    }
    public function getStoreCard(){
        echo    '<div class="card text-white bg-success mb-3;">';
        echo        '<div class="card-header">';
        echo            $this->title;
        echo                    '</div>';
        echo           '<div class="card-body">';
        echo            '<h5 class="card-title">';
        echo                'PHONE';
        echo                '</h5>';
        echo                '<p class="card-text">';
        echo                ' <img src="/svg/phone.svg" alt="phone image" height="300px" hspace="50px"> <br> ';
        echo                parent::getStoreCard() . '<br>';
        echo                ' </p>';
        echo             '</div>';
        echo          '</div>';    
    }
}
?>