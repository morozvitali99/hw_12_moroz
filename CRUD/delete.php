<?php
    require_once   $_SERVER['DOCUMENT_ROOT'] . '/classes/store.class.php';
    include_once   $_SERVER['DOCUMENT_ROOT'] . "/template/header.php";
    require_once   $_SERVER['DOCUMENT_ROOT'] . "/database/connect.php";
            if (empty($_POST['id']))
            {
            header('Location:/');
            }
        $id=$_POST['id'];
        
        StoreClass::delete($id, $db);
?>

        <div
            class="alert alert-warning"
            role="alert">
        <?='Entry deleted, '?>
            <a
            href="/index.php">Homepage
            </a>
        </div>
<?php
    include_once  $_SERVER['DOCUMENT_ROOT'] . "/template/footer.php";
?>