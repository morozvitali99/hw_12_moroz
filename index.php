<?php
    require_once   $_SERVER['DOCUMENT_ROOT'] . '/classes/store.class.php';
    require_once   $_SERVER['DOCUMENT_ROOT'] . '/classes/laptop.store.class.php';
    require_once   $_SERVER['DOCUMENT_ROOT'] . '/classes/phone.store.class.php';
    require_once   $_SERVER['DOCUMENT_ROOT'] . '/classes/watch.store.class.php';
    require_once   $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
    include_once   $_SERVER['DOCUMENT_ROOT'] . "/template/header.php";
    try {
         $sql = "SELECT
                        title,
                        price,
                        description_,
                        id,
                        type_
        FROM store";
        }
          catch(Exception $exception)
              {
              echo 'something get wrong message';
              echo $Exception->getMessage();
              die();
              }


?>

    <div class="container-fluid col-4">
        <h3> The Best Store Items  </h3>
<?php
      try {
            $storeArray = $db->query($sql);
          }
            catch(Exception $ex)
              {
?>

<!-- вывод ошибки о пустой таблице -->
                 <div class="alert alert-warning"
                      role="alert">
                      <?='table store empty, please create table bd '?>
                 <a   href="/database/create_db.php">CREATE DB</a>
                 </div>
<!-- конец вывода ошибки -->

<?php
            die();
              }
// через цикл добавили 3 типа товаров
              foreach ($storeArray as $store)
      {
// определяем тип
              if ($store['type_'] === 'laptop')
          {
              $writer = new LaptopStoreClass ($store['title'],
                                              $store['price'],
                                              $store['description_'],
                                              $store['id'],
                                              $store['type_']
                                             );
            echo $writer->getStoreCard();
          };
              if ($store['type_'] === 'phone')
          {
              $writer = new PhoneStoreClass  ($store['title'],
                                              $store['price'],
                                              $store['description_'],
                                              $store['id'],
                                              $store['type_']
                                             );
            echo $writer->getStoreCard();
          };
              if ($store['type_'] === 'watch')
          {
              $writer = new WatchStoreClass ($store['title'],
                                             $store['price'],
                                             $store['description_'],
                                             $store['id'],
                                             $store['type_']
                                            );
            echo $writer->getStoreCard();
          };
            echo '<br>';
      };
    // конец цикла и вывода на экран товаров
?>
    </div>
<?php

    include_once  $_SERVER['DOCUMENT_ROOT'] . "/template/footer.php";
?>